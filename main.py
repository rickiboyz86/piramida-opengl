from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

def init():

    glClearColor(0., 0., 0., 0.)
    gluOrtho2D(-20.0, 20.0, -20.0, 20.0)
    glEnable(GL_DEPTH_TEST)
    glPointSize(15)


def lines():

    glBegin(GL_LINES)

    glColor3f(1,0,0)
    glVertex3f(10,0,0)
    glVertex3f(-10,0,0)

    glColor3f(0,1,0)
    glVertex3f(0,10,0)
    glVertex3f(0,-10,0)

    glColor3f(0,0,1)
    glVertex3f(0,0,10)
    glVertex3f(0,-0,-10)    
    
    glEnd()

def display():

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    glTranslatef(0, 0, -7.0)
    glRotatef(45, 0.0, 1.0, 0.0)

    lines()

    glBegin(GL_TRIANGLES)

    # depan

    glColor3f(214 / 255, 48 / 255, 49 / 255)      
    glVertex3f( 0.0, 1.0, 0.0) 
    glVertex3f(-1.0, -1.0, 1.0) 
    glVertex3f(1.0, -1.0, 1.0)
 
    # kanan

    glColor3f(214 / 255, 48 / 255, 49 / 255)     
    glVertex3f(0.0, 1.0, 0.0)   
    glVertex3f(1.0, -1.0, 1.0)   
    glVertex3f(1.0, -1.0, -1.0)
 
    # belakang

    glColor3f(255 / 255,118 / 255,117 / 255)     
    glVertex3f(0.0, 1.0, 0.0) 
    glVertex3f(1.0, -1.0, -1.0)   
    glVertex3f(-1.0, -1.0, -1.0)
 
    # kiri

    glColor3f(255 / 255,118 / 255,117 / 255)       
    glVertex3f( 0.0, 1.0, 0.0)  
    glVertex3f(-1.0,-1.0,-1.0)     
    glVertex3f(-1.0,-1.0, 1.0)
   
    glEnd()

    glutSwapBuffers()
    glFlush()

def reshape(width, height):
   aspect = width / height
 
   glViewport(0, 0, width, height)
 
   glMatrixMode(GL_PROJECTION)  
   glLoadIdentity()            
   gluPerspective(45.0, aspect, 0.1, 100.0)

def main():
    glutInit(sys.argv) 
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE) 
    glutInitWindowSize(500, 500) 
    glutInitWindowPosition(100, 100)
    glutCreateWindow("Piramida")
    glutDisplayFunc(display)
    glutReshapeFunc(reshape)
    # glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)
    init() 
    glutMainLoop()

main()